﻿using System;

namespace Client.Domain.Exceptions
{
    public class ErrorCodeException : Exception
    {
        public string ErrorCode { get; }
        public string ExtraMessage { get; }

        public ErrorCodeException(string errorCode, string exceptionMsg = "", Exception inner = null, string extraMessage = "") : base(exceptionMsg, inner)
        {
            ErrorCode = errorCode;
            ExtraMessage = extraMessage;
        }
    }
}
