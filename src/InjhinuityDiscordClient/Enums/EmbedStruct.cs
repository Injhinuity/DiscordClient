﻿namespace Client.Enums
{
    public enum EmbedStruct
    {
        None,
        Success,
        Failure,
        ErrorCode,
        Exception,
        Log,
        List
    }
}
