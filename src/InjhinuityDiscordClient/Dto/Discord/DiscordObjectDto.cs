﻿namespace Client.Dto.Discord
{
    public class DiscordObjectDto : IDiscordObjectDto
    {
        public ulong GuildID { get; set; }
    }
}
