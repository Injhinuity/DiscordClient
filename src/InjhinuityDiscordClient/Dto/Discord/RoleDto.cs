﻿using Client.Domain.Discord.Interfaces;

namespace Client.Dto.Discord
{
    public class RoleDto : DiscordObjectDto, IListable
    {
        public string Name { get; set; }
        public ulong RoleID { get; set; }

        public string ToListString() =>
            $"{Name}";
    }
}
