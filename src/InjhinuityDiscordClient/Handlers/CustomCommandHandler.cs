﻿using Discord.Commands;
using System.Threading.Tasks;
using Client.Domain.Discord.Interfaces;
using Client.Domain.Payload.API;
using Client.Services.Injection;
using Client.Services.Interfaces;
using Client.Dto.Discord;
using Client.Extensions;
using Client.Services.ModuleServices.Interfaces;

namespace Client.Handlers
{
    public class CustomCommandHandler : IService
    {
        private readonly IBotConfig _botConfig;
        private readonly IDiscordPayloadFactory _discordPayloadFactory;
        private readonly IDiscordModuleService _discordModuleService;

        public CustomCommandHandler(IBotConfig botConfig, IDiscordPayloadFactory discordPayloadFactory, IDiscordModuleService discordModuleService)
        {
            _botConfig = botConfig;
            _discordPayloadFactory = discordPayloadFactory;
            _discordModuleService = discordModuleService;
        }

        public async Task<bool> TryHandlingCustomCommand(ICommandContext context, string message)
        {
            if (message[0] != char.Parse(_botConfig.Prefix))
                return false;

            string cleanMsg = message.Substring(1, message.Length - 1);

            var command = new CommandDto { GuildID = context.Guild.Id, Name = cleanMsg };
            var result = await _discordModuleService.Get<CommandAPIPayload>(command);

            if (result.IsSuccessStatusCode)
            {
                var resultCommand = await result.Content.ReadAsJsonAsync<CommandDto>();
                await context.Channel.SendMessageAsync(resultCommand.Body);
                return true;
            }

            return false;
        }
    }
}
