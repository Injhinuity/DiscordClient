﻿using Discord;
using Discord.Commands;
using Client.Domain.Discord.Interfaces;
using Client.Domain.Payload.API;
using Client.Dto.Discord;
using Client.Services.Interfaces;
using Client.Services.ModuleServices.Interfaces;
using System.Threading.Tasks;

namespace Client.Modules
{
    public class ReactionModule : ListableBotModuleBase<ReactionAPIPayload, ReactionDto>
    {
        public ReactionModule(IBotConfig botConfig, IDiscordModuleService discordModuleService, IEmbedService embedService, 
                              IEmbedPayloadFactory embedPayloadFactory, IResources resources) 
            : base(botConfig, discordModuleService, embedService, embedPayloadFactory, resources)
        {
        }

        [Command("rc create"), RequireUserPermission(GuildPermission.BanMembers)]
        public async Task Create()
        {

        }
    }
}
