﻿using System.Threading.Tasks;
using Discord.Commands;
using Discord;
using Client.Dto.Discord;
using Client.Domain.Payload.API;
using Client.Domain.Discord.Interfaces;
using Client.Services.ModuleServices.Interfaces;
using Client.Dto;
using Client.Services.Interfaces;

namespace Client.Modules
{
    public class ChannelModule : ListableBotModuleBase<ChannelAPIPayload, ChannelDto>
    {
        private readonly IChannelNameService _channelService;

        public ChannelModule(IDiscordModuleService discordModuleService, IChannelNameService channelService, IBotConfig botConfig, 
                             IEmbedService embedService, IEmbedPayloadFactory embedPayloadFactory, IResources resources)
            : base(botConfig, discordModuleService, embedService, embedPayloadFactory, resources)
        {
            _channelService = channelService;
        }

        [Command("ch update"), Summary("Updates a given channel with the ID of the channel the command is used in."), RequireUserPermission(GuildPermission.BanMembers)]
        public async Task Update(string shortName)
        {
            var name = _channelService.GetChannelName(shortName);
            await Put("put", GetDto(Context.Guild.Id, name, shortName, Context.Channel.Id));
        }

        [Command("ch reset"), Summary("Resets a given channel ID."), RequireUserPermission(GuildPermission.BanMembers)]
        public async Task Reset(string shortName)
        {
            var name = _channelService.GetChannelName(shortName);
            await Put("put", GetDto(Context.Guild.Id, name, shortName));
        }

        [Command("ch list"), Summary("Lists every channel configuration for the current guild."), RequireUserPermission(GuildPermission.BanMembers)]
        public async Task List()
        {
            var result = await GetAll(GetDto(Context.Guild.Id));

            if (result.IsSuccessStatusCode)
            {
                CreateAndSendList(result.Content);
                return;
            }

            await HandleAPIResult(result, "get");
        }

        private IDiscordObjectDto GetDto(ulong guildID, string name = "", string shortName = "", ulong channelID = 0) =>
            new ChannelDto { GuildID = guildID, Name = name, ShortName = shortName, ChannelID = channelID };
    }
}
