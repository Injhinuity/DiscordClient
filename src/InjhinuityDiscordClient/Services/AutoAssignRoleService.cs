﻿using Discord.WebSocket;
using Client.Dto.Discord;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using Client.Services.ModuleServices.Interfaces;
using Client.Domain.Payload.API;
using Client.Services.Interfaces;

namespace Client.Services
{
    public class AutoAssignRoleService : IAutoAssignRoleService
    {
        private readonly IDiscordModuleService _discordModuleService;
        private readonly IDiscordPayloadFactory _discordPayloadFactory;
        private readonly IParamNameService _paramService;

        public AutoAssignRoleService(IDiscordPayloadFactory discordPayloadFactory, DiscordSocketClient client, IParamNameService paramService,
                                     IDiscordModuleService discordModuleService)
        {
            _discordPayloadFactory = discordPayloadFactory;
            _paramService = paramService;
            _discordModuleService = discordModuleService;

            client.UserJoined += UserJoinedGuild;
        }

        private async Task UserJoinedGuild(SocketGuildUser arg)
        {
            var dataAAR = new ParamDto { GuildID = arg.Guild.Id, ShortName = "aar" };
            var resultAAR = await _discordModuleService.Get<ParamAPIPayload>(dataAAR);

            if (!resultAAR.IsSuccessStatusCode)
                return;

            var dataAARID = new ParamDto { GuildID = arg.Guild.Id, ShortName = "aarid" };
            var resultAARID = await _discordModuleService.Get<ParamAPIPayload>(dataAARID);

            if (!resultAARID.IsSuccessStatusCode)
                return;

            var paramReturn = JsonConvert.DeserializeObject<ParamDto>(await resultAARID.Content.ReadAsStringAsync());
            if (!ulong.TryParse(paramReturn.Value, out var value))
                return;

            var role = arg.Guild.Roles.FirstOrDefault(x => x.Id == value);
            if (role != null)
                await arg.AddRoleAsync(role);
        }
    }
}
