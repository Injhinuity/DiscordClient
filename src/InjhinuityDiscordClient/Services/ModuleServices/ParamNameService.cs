﻿using System.Collections.Generic;
using Client.Domain.Discord.Interfaces;
using Client.Services.Interfaces;
using Client.Services.ModuleServices.Interfaces;
using Newtonsoft.Json;

namespace Client.Services.ModuleServices
{
    public class ParamNameService : IParamNameService
    {
        private readonly IFileReader _fileReader;
        private readonly Dictionary<string, string> _params;

        public ParamNameService(IFileReader fileReader, IBotConfig botConfig)
        {
            _fileReader = fileReader;

            var json = fileReader.ReadFile(botConfig.GuildParamsPath);
            _params = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }

        public string GetParamName(string shortName)
        {
            var name = _params.GetValueOrDefault(shortName);
            return name != "" ? name : false.ToString(); 
        }

    }
}
