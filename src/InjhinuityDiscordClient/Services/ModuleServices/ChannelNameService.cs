﻿using Client.Domain.Discord.Interfaces;
using Client.Services.Interfaces;
using Client.Services.ModuleServices.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Client.Services.ModuleServices
{
    public class ChannelNameService : IChannelNameService
    {
        private readonly IFileReader _fileReader;
        private readonly Dictionary<string, string> _channels;

        public ChannelNameService(IFileReader fileReader, IBotConfig botConfig)
        {
            _fileReader = fileReader;

            var json = _fileReader.ReadFile(botConfig.ChannelIDSPath);
            _channels = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }

        public string GetChannelName(string shortName) =>
            _channels.GetValueOrDefault(shortName);
    }
}
