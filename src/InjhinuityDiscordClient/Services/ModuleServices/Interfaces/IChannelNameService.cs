﻿using Client.Services.Injection;

namespace Client.Services.ModuleServices.Interfaces
{
    public interface IChannelNameService : IService
    {
        string GetChannelName(string shortName);
    }
}
