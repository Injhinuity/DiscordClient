﻿using Client.Services.Injection;

namespace InjhinuityDiscordClient.Services.ModuleServices.Interfaces
{
    public interface ICommandService : IService
    {
        bool ValidateCommandArgs(string name);
    }
}
