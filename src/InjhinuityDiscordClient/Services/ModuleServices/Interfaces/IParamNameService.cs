﻿using Client.Services.Injection;

namespace Client.Services.ModuleServices.Interfaces
{
    public interface IParamNameService : IService
    {
        string GetParamName(string shortName);
    }
}
