﻿using Discord;
using Client.Services.Injection;
using System.Collections.Generic;

namespace Client.Services.ModuleServices.Interfaces
{
    public interface IHelpService : IService
    {
        List<(string, string, bool)> GetCommandInfos(GuildPermissions perms);
    }
}
