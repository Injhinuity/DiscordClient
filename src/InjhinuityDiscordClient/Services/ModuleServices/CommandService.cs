﻿using InjhinuityDiscordClient.Services.ModuleServices.Interfaces;

namespace InjhinuityDiscordClient.Services.ModuleServices
{
    public class CommandService : ICommandService
    {
        private const int COMMAND_NAME_LENGTH = 20;

        public bool ValidateCommandArgs(string name) =>
            name.Length < COMMAND_NAME_LENGTH;
    }
}
