﻿using Client.Services.Injection;
using System.Threading.Tasks;

namespace Client.Services.Interfaces
{
    public interface IBotStartupService : IService
    {
        Task SynchroniseGuilds();
    }
}
