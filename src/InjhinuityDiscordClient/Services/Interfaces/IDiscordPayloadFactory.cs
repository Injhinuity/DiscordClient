﻿using Client.Domain.Payload.API;
using Client.Dto;
using Client.Services.Injection;

namespace Client.Services.Interfaces
{
    public interface IDiscordPayloadFactory : IService
    {
        T CreateDiscordObjectPayload<T>(IDiscordObjectDto data) where T : IAPIPayload;
    }
}
