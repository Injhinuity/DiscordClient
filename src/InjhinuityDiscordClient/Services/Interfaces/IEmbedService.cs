﻿using Discord;
using Client.Domain.Payload.Embed.Interfaces;
using Client.Services.Injection;

namespace Client.Services.Interfaces
{
    public interface IEmbedService : IService
    {
        EmbedBuilder CreateBaseEmbed(IEmbedPayload payload);
        EmbedBuilder CreateFieldEmbed(IEmbedPayload payload);
    }
}
