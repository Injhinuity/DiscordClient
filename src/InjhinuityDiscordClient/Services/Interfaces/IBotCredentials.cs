﻿using Discord;
using Client.Services.Injection;
using System.Collections.Immutable;

namespace Client.Services.Interfaces
{
    public interface IBotCredentials : IService
    {
        ulong ClientID { get; }
        string Token { get; }
        string ApiClientID { get; }
        string ApiClientSecret { get; }
        ImmutableArray<ulong> OwnerIDs { get; }

        bool IsOwner(IUser u);
    }
}
