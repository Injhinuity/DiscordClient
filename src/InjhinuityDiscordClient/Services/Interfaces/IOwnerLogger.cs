﻿using Client.Domain.Exceptions;
using Client.Services.Injection;
using System;
using System.Threading.Tasks;

namespace Client.Services.Interfaces
{
    public interface IOwnerLogger : IService
    {
        Task SetOwnerDMChannel();
        Task LogException(Exception ex);
        Task LogBotException(BotException ex);
        Task LogError(string errorCode, string extraMsg = "", params string[] args);
    }
}
