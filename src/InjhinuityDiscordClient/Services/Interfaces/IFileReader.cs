﻿using Client.Services.Injection;

namespace Client.Services.Interfaces
{
    public interface IFileReader : IService
    {
        string ReadFile(string path);
    }
}
