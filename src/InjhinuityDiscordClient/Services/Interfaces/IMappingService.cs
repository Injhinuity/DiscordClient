﻿using Client.Dto;
using Client.Dto.Interfaces;
using Client.Services.Injection;

namespace Client.Services.Interfaces
{
    public interface IMappingService : IService
    {
        T Map<T>(IDiscordObjectDto data);
        T Map<T>(IEmbedDto data);
    }
}
