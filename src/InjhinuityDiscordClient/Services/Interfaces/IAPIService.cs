﻿using Client.Domain.Payload.API;
using Client.Services.Injection;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client.Services.Interfaces
{
    public interface IAPIService : IService
    {
        Task<HttpResponseMessage> GetAllAsync(IAPIPayload payload);
        Task<HttpResponseMessage> GetAsync(IAPIPayload payload);
        Task<HttpResponseMessage> PostAsync(IAPIPayload payload);
        Task<HttpResponseMessage> PutAsync(IAPIPayload payload);
        Task<HttpResponseMessage> DeleteAsync(IAPIPayload payload);
    }
}
