﻿using Client.Enums;
using Client.Services.Injection;

namespace Client.Services.Interfaces
{
    public interface IColorConverter : IService
    {
        string GetColorCodeFromPayloadType(EmbedPayloadType type);
    }
}
