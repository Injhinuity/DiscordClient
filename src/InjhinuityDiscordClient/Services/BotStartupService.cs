﻿using Discord.WebSocket;
using Client.Domain.Payload.API;
using Client.Dto.Discord;
using Client.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Client.Services.ModuleServices.Interfaces;

namespace Client.Services
{
    public class BotStartupService : IBotStartupService
    {
        private readonly DiscordSocketClient _client;
        private readonly IOwnerLogger _ownerLogger;
        private readonly IDiscordPayloadFactory _discordPayloadFactory;
        private readonly IDiscordModuleService _discordModuleService;

        public BotStartupService(DiscordSocketClient client, IDiscordPayloadFactory discordPayloadFactory, IOwnerLogger ownerLogger,
                                 IDiscordModuleService discordModuleService)
        {
            _client = client;
            _discordPayloadFactory = discordPayloadFactory;
            _discordModuleService = discordModuleService;
            _ownerLogger = ownerLogger;
        }

        public async Task SynchroniseGuilds()
        {
            var failedGuilds = new List<string>();

            foreach (var guild in _client.Guilds)
            {
                var data = new GuildDto { GuildID = guild.Id };
                var result = await _discordModuleService.Post<GuildAPIPayload>(data);

                if (!result.IsSuccessStatusCode)
                    failedGuilds.Add(guild.Name);
            }

            if (failedGuilds.Count > 0)
                await _ownerLogger.LogError("0005", failedGuilds.ToString());
        }
    }
}
