﻿using Client.Domain.Discord.Interfaces;
using Client.Domain.Payload.API;
using Client.Dto;
using Client.Services.Interfaces;

namespace Client.Services
{
    public class DiscordPayloadFactory : IDiscordPayloadFactory
    {
        private readonly IBotConfig _botConfig;
        private readonly IMappingService _mapper;

        public DiscordPayloadFactory(IBotConfig botConfig, IMappingService mapper)
        {
            _botConfig = botConfig;
            _mapper = mapper;
        }

        public T CreateDiscordObjectPayload<T>(IDiscordObjectDto data) where T : IAPIPayload =>
            _mapper.Map<T>(data);
    }
}
